﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationAutoDestroy : MonoBehaviour
{
    public float delayBeforeDestroy = 1.5f;

    void Start()
    {
        // destroying the animation when its finished
        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delayBeforeDestroy);
    }
}