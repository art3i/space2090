﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour
{
    private float speed = 2.0f;
    private int direction = 1;

    void Start()
    {
        direction = Random.Range(1, 4);
    }

    void Update()
    {
        speed = Random.Range(0.5f, 1.5f);

        transform.Translate(Vector3.down * speed * Time.deltaTime);

        if (direction == 1)
        {
            transform.Translate(Vector3.right * 0.3f * Time.deltaTime);
        }

        if (direction == 2)
        {
            transform.Translate(Vector3.left * 0.5f * Time.deltaTime);
        }


        // cleanup when out of view
        if (transform.position.y < -9.0f)
        {
            Destroy(gameObject);
        }
    }
}