﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    private Material backgroundSpriteMaterial;
    private float bgScrollSpeed = 0.2f;

    void Start()
    {
        backgroundSpriteMaterial = transform.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Time.time * bgScrollSpeed;

        if (backgroundSpriteMaterial)
        {
            backgroundSpriteMaterial.mainTextureOffset = new Vector2(0, offset);
        }
    }
}