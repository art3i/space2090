﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    private float speed = 2.0f;
    private float minLife = 1;
    private float maxLife = 3;
    private float nextFiringTime;

    [SerializeField] private GameObject enemyPrefabExplosionAnimation;

    [SerializeField] private GameObject shipDamageAnimation;

    [SerializeField] private GameObject enemyBombPrefab;

    private UIManager uiManager;
    [SerializeField] private AudioClip playerDamageSound;

    void Start()
    {
        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        nextFiringTime = 0.1f;
        shipDamageAnimation.SetActive(false);
    }

    void Update()
    {
        // flying enemy ship from top to bottom
        transform.Translate(Vector3.down * speed * Time.deltaTime);

        // survived enemy ship respawn back on after going out of viewport
        // otherwise destroyed during collision.
        if (transform.position.y < -7.0f)
        {
            transform.position = new Vector3(Random.Range(-7.5f, 7.5f), 7.5f, 0);
        }

        StartFiring();
    }

    // firing bomb from enemy ship when the ship is within viewport
    private void StartFiring()
    {
        float yPosition = transform.position.y;

        if (yPosition < 5.5f && yPosition > -5.5f && Time.time > nextFiringTime)
        {
            float randomInterval = Random.Range(0.7f, 1.5f);
            nextFiringTime = Time.time + randomInterval;
            var bombInstance = Instantiate(enemyBombPrefab, transform.position, Quaternion.identity);
            // TODO : Create a pool of bomb
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // player gets damaged if collide with enemy ship
        if (other.CompareTag("Player"))
        {
            var player = other.GetComponent<Player>();
            player.DamagePlayer();
        }

        // enemy ship gets destroyed after 3 hits from player's laser.
        if (other.CompareTag("PlayerLaser"))
        {
            if (maxLife < minLife)
            {
                // destroy enemy ship ; add points for player
                if (uiManager != null)
                {
                    uiManager.UpdateScore(100);
                }

                Instantiate(enemyPrefabExplosionAnimation, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }

            // each laser hit reduce 1 life
            maxLife = maxLife - 1;
            shipDamageAnimation.SetActive(true);
            AudioSource.PlayClipAtPoint(playerDamageSound, Vector3.zero, 1f);
            Destroy(other.gameObject);
        }
    }
}