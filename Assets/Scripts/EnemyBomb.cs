﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBomb : MonoBehaviour
{
    private float speed = 9.0f;
    private int direction = 1;
    [SerializeField] private AudioClip bombExplodeSound;

    void Start()
    {
        direction = Random.Range(1, 3);
    }

    void Update()
    {
        speed = Random.Range(3, 7);

        transform.Translate(Vector3.down * speed * Time.deltaTime);

        if (direction == 1)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }


        // cleanup when out of view
        if (transform.position.y < -6.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // player gets damaged if collide with this enemy bomb
        if (other.CompareTag("Player"))
        {
            var player = other.GetComponent<Player>();
            player.DamagePlayer();
            AudioSource.PlayClipAtPoint(bombExplodeSound, transform.position, 1.0f);
            Destroy(gameObject, 0.2f);
        }
    }
}