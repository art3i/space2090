﻿using System.Collections;
using UnityEngine;

public sealed class GameManager : MonoBehaviour
{
    public bool isGameOver;
    private bool isMissionComplete;
    private bool isMissionActive;
    private bool hasGameStarted;
    private bool isSpawnManagerActive;


    public GameObject player;

    private GameObject playerInstance;

    private SpawnManager _spawnManager;

    //sounds
    [SerializeField] private AudioClip gameOverSound;

    [SerializeField] private AudioClip missionCompleteSound1;
    [SerializeField] private AudioClip missionCompleteSound2;
    [SerializeField] private AudioClip reinforcementSound1;

    // playerMissionDuration is the time in seconds that player must survive to win the game.
    [SerializeField] private float playerMissionDuration;

    // missionStartTime is the Time.time offset : when the player actually starts playing the mission; e.g after briefing is completed
    private float missionStartTime;

    private float actualGameStartTime;
    private bool isOngoingPostMissionCinematics;

    [SerializeField] private ScriptableObjectMissionProperties missionProperties;

    // events for game states
    public delegate void GameInitEventHandler();

    public static event GameInitEventHandler OnGameInit;


    public delegate void GameStartEventHandler();

    public static event GameStartEventHandler OnGameStart;


    public delegate void GameOverEventHandler();

    public static event GameOverEventHandler OnGameOver;


    public delegate void MissionStartEventHandler();

    public static event MissionStartEventHandler OnMissionStart;


    public delegate void MissionStatusUpdatesEventHandler(string newStatus);

    public static event MissionStatusUpdatesEventHandler OnMissionStatusUpdate;

    public delegate void MissionCompleteEventHandler();

    public static event MissionCompleteEventHandler OnMissionComplete;

    public delegate void PostMissionEventHandler();

    public static event PostMissionEventHandler OnPostMissionInit;


    // GameManager as singleton

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gm = new GameObject("GameManager");
                gm.AddComponent<GameManager>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public bool GameOver { get; set; }


    void Start()
    {
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        isMissionActive = false;
        isSpawnManagerActive = false;
        isGameOver = false;
        hasGameStarted = false;
        isOngoingPostMissionCinematics = false;

        OnGameInit();
    }

    void Update()
    {
        if (isMissionActive && Time.time >= (missionStartTime + playerMissionDuration) && playerInstance)
        {
            InitMissionComplete();
        }
        else
        {
            // continue mission and show mission status and progress
            if (isMissionActive && !isGameOver && !isMissionComplete && !isOngoingPostMissionCinematics)
            {
                var text = "Reinforcement ETA " + ((missionStartTime + playerMissionDuration) - Time.time);
                OnMissionStatusUpdate(text);
            }
        }

        // begin mission when the intro is shown and player press space key
        if (!isMissionActive && hasGameStarted && Input.GetKeyDown(KeyCode.X))
        {
            BeginPlayerMission();
        }

        else if (!hasGameStarted && !isMissionActive && Input.GetKeyDown(KeyCode.X))
        {
            // either game is over or the mission complete process has been finished
            // and main-menu is shown

            // cleanup last session instances if any;
            ClearPlayerInstance();
            playerInstance = Instantiate(player, Vector3.zero, Quaternion.identity);

            StartTheGame();
        }

        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
    }

    public void SetDifficultyEasy()
    {
        missionProperties.SetDifficulty();
        playerMissionDuration = missionProperties.missionDuration;
    }

    public void SetDifficultyHard()
    {
        missionProperties.SetDifficulty("Hard");
        playerMissionDuration = missionProperties.missionDuration;
    }

    public void InitGameOver()
    {
        AudioSource.PlayClipAtPoint(gameOverSound, Vector3.zero, 1.0f);
        ClearPlayerInstance();

        _spawnManager.CleanTheSky();
        isGameOver = true;
        hasGameStarted = false;
        isSpawnManagerActive = false;
        isMissionActive = false;
        isMissionComplete = false;
        isOngoingPostMissionCinematics = false;

        GameOver = true;
        // event for game over
        OnGameOver();
    }

    private void StartTheGame()
    {
        hasGameStarted = true;
        isGameOver = false;
        isMissionComplete = false;
        isMissionActive = false;
        actualGameStartTime = Time.time;
        isOngoingPostMissionCinematics = false;

        // sending game start event
        OnGameStart();
    }

    // after cinematic/mission intro is over, this function is called
    private void BeginPlayerMission()
    {
        _spawnManager.InitSpawnManager();
        isMissionActive = true;
        missionStartTime = Time.time;
        OnMissionStart();
    }

    private void ClearPlayerInstance()
    {
        if (playerInstance)
        {
            Destroy(playerInstance);
        }
    }


    private void InitMissionComplete()
    {
        if (isOngoingPostMissionCinematics == false)
        {
            StartCoroutine(ShowPostMissionCinematics());
        }
    }

    private IEnumerator ShowPostMissionCinematics()
    {
        isOngoingPostMissionCinematics = true;
        AudioSource.PlayClipAtPoint(reinforcementSound1, Vector3.zero, 1f);

        playerInstance.GetComponent<Player>().EnableShield();
        _spawnManager.InitAllyPlayers();
        OnMissionStatusUpdate("Reinforcement Arrived!");

        yield return new WaitForSeconds(2f);
        AudioSource.PlayClipAtPoint(missionCompleteSound1, Vector3.zero, 1f);

        yield return new WaitForSeconds(6f);

        OnPostMissionInit();
        _spawnManager.CleanTheSky();

        yield return new WaitForSeconds(5f);
        AudioSource.PlayClipAtPoint(missionCompleteSound2, Vector3.zero, 1f);

        yield return new WaitForSeconds(1.5f);
        OnMissionComplete();
        hasGameStarted = false;
        isGameOver = false;
        isMissionComplete = true;
        isMissionActive = false;
        isOngoingPostMissionCinematics = false;
    }
}