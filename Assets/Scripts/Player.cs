﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float nextFireTime;

    [SerializeField] private float speed;
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private GameObject tripleLaserPrefab;
    [SerializeField] private GameObject shieldPrefab;


    [SerializeField] private GameObject playerPrefabExplosionAnimation;
    [SerializeField] private GameObject shipDamageEffect;


    [SerializeField] private float powerUpTimeOutSeconds = 5.0f;
    [SerializeField] private float powerUpSpeed = 1.0f;


    [SerializeField] private AudioClip firingSound;

    [SerializeField] private AudioClip playerDamageSound;

    public bool canTripleShot;

    private bool isShieldActive = false;

    private GameObject activatedShield;

    private int playerRemainingLife = 3;
    private int playerMinLife = 1;

    private UIManager uiManager;

    private GameManager gameManager;

    public void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        speed = 5.0f;
        nextFireTime = 0.0f;
        canTripleShot = false;

        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();

        if (uiManager != null)
        {
            uiManager.UpdateLives(playerRemainingLife);
        }
    }

    public void Update()
    {
        Movement();

        FireLaser();
    }

    public void FireLaser()
    {
        // fire laser from player position on space key down
        // firing rate = delay to next available fire (nextFireTime)
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFireTime)
        {
            nextFireTime = Time.time + 0.1f;
            Instantiate(laserPrefab,
                new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z),
                Quaternion.identity);

            AudioSource.PlayClipAtPoint(firingSound, transform.position, 1.0f);
        }


        // power up shooting without fire-rate limit
        if (Input.GetKey(KeyCode.Space) && canTripleShot)
        {
            StartCoroutine(TripleShotPowerTimeout());
            Instantiate(tripleLaserPrefab, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(firingSound, transform.position, 1.0f);
        }
    }

    public void AddSpeedBoost(float speedMultiplier)
    {
        StartCoroutine(SpeedUpTimeout());
        powerUpSpeed = speedMultiplier;
    }


    public void DamagePlayer()
    {
        AudioSource.PlayClipAtPoint(playerDamageSound, Vector3.zero, 1f);
        if (isShieldActive == false)
        {
            playerRemainingLife--;
            uiManager.UpdateLives(playerRemainingLife);
            PlayerDamageAnimation();
        }


        if (playerRemainingLife < playerMinLife)
        {
            Instantiate(playerPrefabExplosionAnimation, transform.position, Quaternion.identity);
            // game over as the player died
            if (gameManager != null)
            {
                uiManager.UpdateLives(0);
                gameManager.InitGameOver();
            }
        }
    }

    public void PlayerDamageAnimation()
    {
        shipDamageEffect.SetActive(true);
    }

    public void EnableShield()
    {
        isShieldActive = true;
        activatedShield = Instantiate(shieldPrefab, gameObject.transform);
        StartCoroutine(ShieldTimeout());
    }

    private void Movement()
    {
        var horizontalInput = Input.GetAxis("Horizontal");
        var verticalInput = Input.GetAxis("Vertical");


        transform.Translate(Vector3.right * (speed * powerUpSpeed) * horizontalInput * Time.deltaTime);
        transform.Translate(Vector3.up * (speed * powerUpSpeed) * verticalInput * Time.deltaTime);

        // left -right wrap

        if (transform.position.x > 9.0f)
        {
            transform.position = new Vector3(-9.0f, transform.position.y, 0);
        }

        else if (transform.position.x < -9.0f)
        {
            transform.position = new Vector3(9.0f, transform.position.y, 0);
        }

        // locking vertical movement

        if (transform.position.y > 2.7f)
        {
            transform.position = new Vector3(transform.position.x, 2.7f, 0);
        }
        else if (transform.position.y < -4.0f)
        {
            transform.position = new Vector3(transform.position.x, -4.0f, 0);
        }
    }

    private void ResetSpeedPowerup()
    {
        powerUpSpeed = 1.0f;
    }

    private IEnumerator TripleShotPowerTimeout()
    {
        yield return new WaitForSeconds(powerUpTimeOutSeconds);
        canTripleShot = false;
    }

    private IEnumerator SpeedUpTimeout()
    {
        yield return new WaitForSeconds(powerUpTimeOutSeconds);
        ResetSpeedPowerup();
    }

    private void DestroyShield()
    {
        isShieldActive = false;
        Destroy(activatedShield);
    }

    private IEnumerator ShieldTimeout()
    {
        yield return new WaitForSeconds(15.0f);
        DestroyShield();
    }
}