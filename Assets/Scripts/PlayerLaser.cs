﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaser : MonoBehaviour
{
    private float speed = 10.0f;

    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);

        // cleanup when out of view
        if (transform.position.y > 5.0f)
        {
            Destroy(gameObject);
        }
    }
}