﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

// this script is added to all powerups prefabs.
// SpawnManager script is spawning powerups prefab at random time

public class Powerup : MonoBehaviour
{
    [SerializeField] private float speed;

    // assign id for powerups prefab, 100 = triple shot, 101 = speed boost, 102 = shield
    [SerializeField] private int powerupPrefabID;

    [SerializeField] private AudioClip powerUpSound;

    private enum powerUpIDs
    {
        TripleShot = 100,
        SpeedBoost = 101,
        Shield = 102
    };

    private void Start()
    {
        speed = 3.5f;
    }

    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * speed);

        if (transform.position.y < -6.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;

        var player = other.GetComponent<Player>();

        AudioSource.PlayClipAtPoint(powerUpSound, transform.position, 0.8f);

        switch (powerupPrefabID)
        {
            case (int) powerUpIDs.TripleShot:
                player.canTripleShot = true;
                Destroy(gameObject);
                break;
            case (int) powerUpIDs.SpeedBoost:
                player.AddSpeedBoost(1.6f);
                Destroy(gameObject);
                break;
            case (int) powerUpIDs.Shield:
                player.EnableShield();
                Destroy(gameObject);
                break;
        }
    }
}