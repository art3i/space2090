﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewMissionProperties", menuName = "Create Mission")]
public class ScriptableObjectMissionProperties : ScriptableObject
{
    public string missionName;

    // in seconds
    public float missionDuration;

    [TextArea(7,15)] public string missionIntroBreif;
    [TextArea(5,7)] public string missionEndBreif;

    // Mission difficulty is the duration for which player needs to survive.
    public void SetDifficulty(string difficulty = "Easy")
    {
        if (difficulty == "Easy")
        {
            missionDuration = 60.0f;
        }
        else if (difficulty == "Hard")
        {
            if (missionDuration > 0)
            {
                missionDuration = missionDuration * 5;
            }
            else
            {
                missionDuration = 120.5f;
            }
        }
        else
        {
            missionDuration = 40.5f;
        }
    }
}