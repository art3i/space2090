﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject enemyShipPrefab;

    [SerializeField] private GameObject[] powerUpsPrefab;

    [SerializeField] private GameObject asteroidsPrefab;

    [SerializeField] private GameObject allyPlayerPrefab;

    List<GameObject> generatedPrefabsList = new List<GameObject>();

    private bool keepGenerating = false;

    private IEnumerator generatorCoroutine;


    public void InitSpawnManager()
    {
        keepGenerating = true;
        generatorCoroutine = StartPrefabGenerator();
        StartCoroutine(generatorCoroutine);
    }

    public void CleanTheSky()
    {
        keepGenerating = false;
        foreach (var element in generatedPrefabsList)
        {
            Destroy(element);
        }

        StopCoroutine(generatorCoroutine);
    }

    public void Restart()
    {
        CleanTheSky();
        keepGenerating = true;
        StartCoroutine(generatorCoroutine);
    }

    public void InitAllyPlayers()
    {
        var ship1 = Instantiate(allyPlayerPrefab, new Vector3(-4.5f, -2.5f, 0), Quaternion.identity);
        var player1 = ship1.GetComponent<Player>();
        player1.EnableShield();
        generatedPrefabsList.Add(ship1);

        var ship2 = Instantiate(allyPlayerPrefab, new Vector3(4.5f, -2.5f, 0), Quaternion.identity);
        var player2 = ship2.GetComponent<Player>();
        player2.EnableShield();
        generatedPrefabsList.Add(ship2);
    }

    private void Spawn()
    {
        var enemyShipInstance = Instantiate(enemyShipPrefab, new Vector3(Random.Range(-7.5f, 7.5f), 11.5f, 0),
            Quaternion.identity);

        generatedPrefabsList.Add(enemyShipInstance);

        // spawn random powerups (0, 1 or 2)
        var powerupsIndex = Random.Range(0, 3);
        var xPosition = Random.Range(-7.5f, 7.5f);
        var yPosition = Random.Range(7.5f, 17.5f);

        var powerUpsInstance = Instantiate(powerUpsPrefab[powerupsIndex],
            new Vector3(xPosition, yPosition, 0), Quaternion.identity);

        generatedPrefabsList.Add(powerUpsInstance);

        SpawnAsteroids();
    }

    private void SpawnAsteroids()
    {
        var xPosition = Random.Range(-0.2f, 3.5f);
        var yPosition = Random.Range(6.5f, 9.5f);

        var asteroid = Instantiate(asteroidsPrefab,
            new Vector3(xPosition, yPosition, 0), Quaternion.identity);
    }

    private IEnumerator StartPrefabGenerator()
    {
        while (keepGenerating)
        {
            var waitTime = Random.Range(5, 9);
            yield return new WaitForSeconds(waitTime);
            Spawn();
        }
    }
}