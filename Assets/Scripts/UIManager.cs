﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Sprite[] lives;

    public Image lifeImage;

    public GameObject mainMenu;

    public GameObject missionConsole;

    public GameObject missionIntroVideo;

    public GameObject missionEndVideo;

    public Text playerScoreUI;

    public TextMeshProUGUI playerInstruction;

    public TextMeshProUGUI missionStatusText;

    public TextMeshProUGUI missionBriefingText;

    public int playerCurrentLifeCount = 3;

    private int playerScoreCounter = 000;

    private bool shouldTextBlink;

    private IEnumerator blinkTextCoroutineInstance;
    private IEnumerator missionBriefCoroutineInstance;

    [SerializeField] private ScriptableObjectMissionProperties missionProperties;

    //sounds

    [SerializeField] private AudioClip incomingTransmission;

    [SerializeField] private AudioClip beginMission;


    private void Awake()
    {
        GameManager.OnGameStart += StartGame;
        GameManager.OnGameOver += OnGameOver;
        GameManager.OnGameInit += ShowStartMenu;
        GameManager.OnMissionStatusUpdate += UpdateMissionStatus;
        GameManager.OnMissionStart += OnMissionBegin;
        GameManager.OnMissionComplete += MissionComplete;
        GameManager.OnPostMissionInit += ShowPostMissionCinematics;
    }

    public void UpdateLives(int currentLifeCount)
    {
        playerCurrentLifeCount = currentLifeCount;

        lifeImage.sprite = lives[currentLifeCount];

        StartCoroutine(EnableBlinkingHealthBar());
    }

    public void UpdateScore(int point = 10)
    {
        playerScoreCounter += point;
        playerScoreUI.text = "Score: " + playerScoreCounter.ToString();
    }


    public void StartGame()
    {
        HideStartMenu();
        StartCoroutine(InitMissionIntro());
        ResetDefault();
    }

    public void StartMissionBriefing()
    {
        var instruction = "\n \n [ Press X to begin ] ";

        var brief = missionProperties.missionIntroBreif + instruction;

        missionConsole.SetActive(true);
        missionIntroVideo.SetActive(true);
        missionEndVideo.SetActive(false);
        missionBriefCoroutineInstance = WriteMissionBrief(brief);
        StartCoroutine(missionBriefCoroutineInstance);
    }

    public void ShowPostMissionCinematics()
    {
        AudioSource.PlayClipAtPoint(incomingTransmission, Vector3.up, 0.8f);
        var brief = missionProperties.missionEndBreif;

        missionConsole.SetActive(true);
        missionIntroVideo.SetActive(false);
        missionEndVideo.SetActive(true);
        StartCoroutine(WriteMissionBrief(brief));
    }

    public void OnMissionBegin()
    {
        missionConsole.SetActive(false);
        StopCoroutine(missionBriefCoroutineInstance);
        missionStatusText.fontSize = 10.0f;
        AudioSource.PlayClipAtPoint(beginMission, Vector3.zero, 1f);
    }


    public void OnGameOver()
    {
        playerInstruction.text = "GAME OVER \n Press X to restart";
        shouldTextBlink = true;
        ShowStartMenu();
    }

    public void MissionComplete()
    {
        missionStatusText.text = "Mission Complete!! \n" + "Your Score : " + playerScoreCounter;
        missionStatusText.fontSize = 15.5f;
        ShowReplayMenu();
    }

    public void ShowReplayMenu()
    {
        playerInstruction.text = "Press X key to play again ...";
        shouldTextBlink = false;
        StartCoroutine(blinkTextCoroutineInstance);
        ShowStartMenu();
    }

    public void ClearMissionStatus()
    {
        missionStatusText.text = "";
        missionStatusText.fontSize = 8.0f;
    }

    public void ClearMenuInstruction()
    {
        playerInstruction.text = "";
    }

    public void UpdateMissionStatus(string content = "")
    {
        missionStatusText.text = content;
        missionStatusText.fontSize = 11.5f;
    }


    public void ShowStartMenu()
    {
        missionConsole.SetActive(false);
        mainMenu.SetActive(true);

        blinkTextCoroutineInstance = EnableBlinkingInstruction();
        StartCoroutine(blinkTextCoroutineInstance);
    }


    private void HideStartMenu()
    {
        mainMenu.SetActive(false);
        StopCoroutine(blinkTextCoroutineInstance);
    }

    private void ResetDefault()
    {
        UpdateLives(0);
        playerScoreCounter = 0;
        UpdateScore(0);
        ClearMissionStatus();
    }

    private IEnumerator EnableBlinkingHealthBar()
    {
        while (playerCurrentLifeCount == 1)
        {
            lifeImage.sprite = lives[0]; // sprite for no-life
            yield return new WaitForSeconds(0.5f);
            lifeImage.sprite = lives[1]; // sprite for single life
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator EnableBlinkingInstruction()
    {
        var currentInstruction = playerInstruction.text;
        while (shouldTextBlink)
        {
            playerInstruction.text = "";
            yield return new WaitForSeconds(0.9f);
            playerInstruction.text = currentInstruction;
            yield return new WaitForSeconds(0.9f);
        }
    }

    private IEnumerator InitMissionIntro()
    {
        var delay = 1.5f;
        AudioSource.PlayClipAtPoint(incomingTransmission, Vector3.zero, 1f);
        yield return new WaitForSeconds(delay);
        StartMissionBriefing();
    }

    private IEnumerator WriteMissionBrief(string content = "")
    {
        var delayBetweenChar = 0.04f;

        for (int i = 0; i < content.Length; i++)
        {
            var currentText = content.Substring(0, i);
            missionBriefingText.text = currentText;
            yield return new WaitForSeconds(delayBetweenChar);
        }
    }
}